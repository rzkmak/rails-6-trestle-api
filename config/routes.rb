Trestle::Engine.routes.draw do
  resources :articles, only: %i[new edit], module: 'articles_admin', controller: 'admin'
end

Rails.application.routes.draw do
  resources :articles
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
